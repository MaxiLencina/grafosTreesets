package Trees;

import java.util.ArrayList;
import java.util.TreeSet;

public class DiGrafo {

	private ArrayList<TreeSet <Integer>> vertices;
	double [][] pesos;
	private double pesoTotal = 0;
	
	public DiGrafo(int n){
		this.vertices = new  ArrayList<TreeSet <Integer>> ();
		for (int i = 0 ; i < n ; i++) {
			this.vertices.add(new TreeSet<Integer>());
		}
		this.pesos = new double [n][n];
	}
	
	public void setArista(int i, int j,double peso){
		checkBounds(i, j);
		this.vertices.get(i).add(j);
		pesos[i][j] = peso;
		pesoTotal += peso;
	}

	
	public void deleteArista(int i, int j){
		checkBounds(i, j);
		if(areNeighbours(i,j))
			this.vertices.get(i).remove(j);
		pesoTotal -= pesos[i][j];
		this.pesos[i][j] = 0;
	}
	
	public ArrayList<TreeSet<Integer>> vertices(){
		return this.vertices;
	}
	
	
	public boolean areNeighbours(int i, int j){
		checkBounds(i, j);
		boolean containsI = this.vertices.get(i).contains(j);
		return containsI  ? true : false;
	}
	
	
	public TreeSet<Integer> neighborhood(int i){
		checkBounds(i,0);
		return this.vertices.get(i);
	}
	
	public int grado(int i){
		checkBounds(i,0);
		return this.vertices.get(i).size();
	}
	
	
	public boolean isConex(){
		for(int i = 0; i< this.vertices.size(); i++){
			if(this.grado(i) == 0)
				return false;
		}
		return true; 
	}
	
	
	public int cantCompConexas(){
		int cont = 0;
		for(int i = 0; i< this.vertices.size(); i++){
			if(this.grado(i) == 0)
				cont++;
		}
		return cont;
	}
	
	public int cantVertices(){
		return this.vertices.size();
	}
	
	public double getPeso(int i, int j){
		this.checkBounds(i,j);
		return this.pesos[i][j];
	}
	
	public boolean esHoja(int i){
		this.checkBounds(i,0);
		return this.grado(i) == 1;
	}
	
	public double longitud(){
		return this.pesoTotal;
	}
	

	public String toString(){
		String grafo = "";
		for(int i = 0; i < this.vertices.size(); i++){
			grafo +="V�rtice: "+ i + " Vecinos: "+this.vertices.get(i).toString() +"\n";
		}
		return grafo;
	}
	
	void checkBounds(int i, int j) {
		if(i >= this.vertices.size() || i <= -1 || j >= this.vertices.size() || j <= -1)
			throw new IllegalArgumentException("Out of bounds");
	}
	
}
