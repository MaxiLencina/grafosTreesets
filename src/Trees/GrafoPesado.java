package Trees;

import java.util.TreeSet;

public class GrafoPesado {

	private Grafo grafo;
	private double [] [] pesos;
	private double pesoTotal;
	
	public GrafoPesado(int n){
		this.grafo = new Grafo(n);
		this.pesos = new double [n][n];
	}
	
	
	public Grafo getGrafo(){
		return grafo;
	}
	
	public void setArista(int i,int j, double peso){
		this.grafo.setArista(i, j);
		this.pesos[i][j] = this.pesos[j][i] = peso;
		this.pesoTotal += peso;
	}
	
	
	public void deleteArista(int i, int j){
		this.grafo.deleteArista(i, j);
		this.pesoTotal -= this.pesos[i][j];
		this.pesos[i][j] = this.pesos[j][i] = 0;
		
	}
	
	public double getPeso(int i, int j){
		this.grafo.checkBounds(i,j);
		return this.pesos[i][j];
	}
	
	public boolean esHoja(int i){
		this.grafo.checkBounds(i,0);
		return this.grafo.grado(i) == 1;
	}
	
	public double longitud(){
		return this.pesoTotal;
	}
	
	public int cantVertices(){
		return grafo.cantVertices();
	}
	
	public boolean areNeighbours(int i,int j){
		return this.grafo.areNeighbours(i, j);
	}
	
	public TreeSet<Integer> neighberhood(int i){
		return this.grafo.neighborhood(i);
	}
	
	
	public String toString(){
		return this.grafo.toString();
	}


	


}
