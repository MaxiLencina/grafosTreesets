package Trees;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Ignore;
import org.junit.Test;

public class GrafoPesadoTest {

	@Ignore
	public void constructorTest() {
		GrafoPesado g = constructor();
		System.out.println(g);
	}

	private GrafoPesado constructor() {
		GrafoPesado g = new GrafoPesado (7);
		g.setArista(0, 1,8);
		g.setArista(0, 2,4);
		g.setArista(1, 2,10);
		g.setArista(1, 3,2);
		g.setArista(1, 4,3);
		g.setArista(2, 4,8);
		g.setArista(3, 4,1);
		g.setArista(4, 6,4);
		g.setArista(3, 5,5);
		g.setArista(5, 6,6);
		g.setArista(3, 6,6);
		return g;
	}
	
	@Ignore
	public void deleteTest(){
		GrafoPesado g = constructor();
		g.deleteArista(0,1);
		g.deleteArista(0,2);
		g.deleteArista(2,2);
		g.deleteArista(1,2);
		System.out.println(g);
	}
	
	@Ignore
	public void vecinosTest(){
		GrafoPesado g = constructor();
		assertTrue(g.areNeighbours(0, 1));
		assertTrue(g.areNeighbours(0, 2));
		assertTrue(g.areNeighbours(2, 1));
		assertTrue(g.areNeighbours(2, 1));
		g.deleteArista(0, 1);
		assertFalse(g.areNeighbours(0, 1));
		
	}
	
	@Test
	public void primTest(){
		GrafoPesado g = constructor();
		GrafoPesado nuevo = GraphAlgorithms.agmPrim(g, 0);
		System.out.println(g);
		System.out.println(nuevo);
	}

}
