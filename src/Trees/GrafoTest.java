/**
 * 
 */
package Trees;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * @author Max
 *
 */
public class GrafoTest {

	@Test
	public void constructorTest() {
		Grafo g = constructor();
		System.out.println(g);
	}

	private Grafo constructor() {
		Grafo g = new Grafo (3);
		g.setArista(0, 1);
		g.setArista(0, 2);
		g.setArista(1, 2);
		g.setArista(2, 2);
		return g;
	}

	
	@Test
	public void deleteTest(){
		Grafo g = constructor();
		g.deleteArista(0,1);
		g.deleteArista(0,2);
		g.deleteArista(2,2);
		g.deleteArista(1,2);
		System.out.println(g);
	}
	
	@Test 
	public void vecinosTest(){
		Grafo g = constructor();
		assertTrue(g.areNeighbours(0, 1));
		assertTrue(g.areNeighbours(0, 2));
		assertTrue(g.areNeighbours(2, 1));
		assertTrue(g.areNeighbours(2, 1));
		g.deleteArista(0, 1);
		assertFalse(g.areNeighbours(0, 1));
		
	}
	
	@Test
	public void vecindarioTst(){
		Grafo g = constructor();
		System.out.println(g.neighborhood(2));
		assertEquals(3,g.grado(2));
	}
	
	@Test
	public void isConex(){
		Grafo g = constructor();
		assertTrue(g.isConex());
		assertEquals(0,g.cantCompConexas());
	}
	
	
	
	
	
	
}
