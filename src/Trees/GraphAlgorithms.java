package Trees;


import java.util.ArrayList;
import java.util.Set;
import java.util.TreeSet;


public class GraphAlgorithms {
	
	
	
	//algoritmo prim
	
	public static GrafoPesado agmPrim(GrafoPesado grafo, int verticeStart){
		
		GrafoPesado arbol = new GrafoPesado(grafo.cantVertices());
		
		TreeSet <Integer> visited = new TreeSet<Integer>();
		visited.add(verticeStart);
	    
		for(int i = 0;i<grafo.cantVertices()-1;i++){
			Arista a = menorArista(grafo, visited); // De un amarillo a un negro
			arbol.setArista(a.amarillo, a.negro, a.peso);
			visited.add(a.negro);
		}
		return arbol;
		
	 }
	 
	// Inner class
		static class Arista {
			public int amarillo;
			public int negro;		
			public double peso;
			
			public Arista(int verticeAmarillo, int verticeNegro, double pesoArista){
				amarillo = verticeAmarillo;
				negro = verticeNegro;
				peso = pesoArista;
			}

			@Override
			public boolean equals(Object obj){
				if (this == obj)
					return true;

				if (obj == null || getClass() != obj.getClass())
					return false;
				
				Arista otra = (Arista) obj;
				return amarillo == otra.amarillo && negro == otra.negro;
			}	
			
			
		}

		// Retorna la arista de menor peso entre un vertice amarillo y uno no amarillo
		static Arista menorArista(GrafoPesado grafo, Set<Integer> amarillos){
			Arista ret = new Arista(0, 0, Double.MAX_VALUE);
			
			for(Integer i: amarillos)
			for(Integer j: grafo.neighberhood(i)) 
				if( amarillos.contains(j) == false ){
					if( grafo.getPeso(i, j) < ret.peso )
						ret = new Arista(i, j, grafo.getPeso(i, j));
				}
			return ret;
		}
		
		
		
}
